package yg.blatt;

import pyutils.Generator;

import java.io.*;

import edu.berkeley.nlp.util.Numberer;

public class LatticeInputsFromProcessedFile extends Generator<LatticeInput> {

	private static Numberer _tagNumberer = Numberer.getGlobalNumberer("tags");

	BufferedReader _reader;
	TypesLexicon _tlex = null;
	boolean _striptags = false;
	int id = 0;

	public LatticeInputsFromProcessedFile(File filename, TypesLexicon tlex, boolean stripTags) throws IOException {
		this._reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(filename), "utf8"));
		this._tlex = tlex;
      this._striptags = stripTags;
	}

	public LatticeInputsFromProcessedFile(BufferedReader reader, TypesLexicon tlex, boolean stripTags)
			throws IOException {
		this._reader = reader;
		this._tlex = tlex;
      this._striptags = stripTags;
	}

	public LatticeInputsFromProcessedFile setLexicon(TypesLexicon tlex) {
		this._tlex = tlex;
		return this;
	}

	public void func() {
		PlainLatticeInput lat = new PlainLatticeInput();
		String line;
		try {
			while ((line = this._reader.readLine()) != null) {
				if (line.trim().isEmpty()) {
					if (lat.length() > 0) {
						this.id+=1;
						lat.setProperty("line", lat.stringRep());
						lat.setProperty("id", "sent"+this.id);
						yield(lat);
					}
					lat = new PlainLatticeInput();
				} else {
					String parts[] = line.trim().split("\\s+");
					int start = Integer.parseInt(parts[0]);
					int end = Integer.parseInt(parts[1]);
               // @@YG: set tokid, verify the lattice data.
					if (!(end > start))
						continue; // try to avoid some input errors
					String form = parts[2];
               String lemma = parts[3];
               String ctag = parts[4];
               String ftag = parts[5];

					String tag = ftag;
               if (_striptags) { tag = tag.split("-")[0]; }
               if (tag.equals("_")) { tag = null; }
               String morph = parts[6];
               String tokid = parts[7];
               double w_t_prob = parts.length > 8 ? Float.valueOf(parts[8]) : -1.0;
               //if (tag!= null && !_tlex.allPossibleTags().contains(_tagNumberer.number(tag))) {
               //   System.err.println("unknown tag [" + tag + "] in lattice, allowing all tags for word");
               //   tag = null;
               //} else {
               //   System.err.println("KNOWN:"+tag);
               //}
					if (tag == null) {
						for (short t : _tlex.possibleNumericTagsForWord(form)) {
							lat.addWordData(start, end, form, t, tokid);
						}
					} else {
						lat.addWordData(start, end, form, tag, w_t_prob, tokid);
					}
				}
			}
		} catch (IOException e) {
			System.err.println("error reading file.");
			e.printStackTrace();
		}
		if (lat.length() > 0) {
			this.id+=1;
			lat.setProperty("line", lat.stringRep());
			lat.setProperty("id", "sent"+this.id);
			yield(lat);
		}
		return;
	}
}
