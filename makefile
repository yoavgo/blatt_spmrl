
parser:
	find java -name '*.java' |xargs javac -d javabin -cp java

nosvn:
	find java -name '.svn' |xargs rm -rf

pack:
	make parser
	make nosvn
	make jar
	tar zcvf blatt.tgz java makefile javabin README.yg blatt.jar

full:
	tar zcvf blatt.full.tgz -C ../ --exclude='*.tgz' blatt

jar:
	jar cvfe blatt.jar edu.berkeley.nlp.PCFGLA.BerkeleyLatticeParser java README.yg -C javabin/ .

all: pack  full

clean:
	rm -rf javabin/*
